CREATE DATABASE  IF NOT EXISTS `highlight_the_web` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `highlight_the_web`;
-- MySQL dump 10.13  Distrib 8.0.12, for macos10.13 (x86_64)
--
-- Host: localhost    Database: highlight_the_web
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `gmail_memory`
--

LOCK TABLES `gmail_memory` WRITE;
/*!40000 ALTER TABLE `gmail_memory` DISABLE KEYS */;
INSERT INTO `gmail_memory` VALUES (1,'msg-f:1631055195753421414','ponvandur@upspring.it','NEUTRAL'),(10,'msg-f:1630679250164798492','ponvandur@upspring.it','NEUTRAL'),(13,'msg-f:1630863462042954464','receipts+EzrL3u1zhwU5zveIWgdK@stripe.com','NEUTRAL');
/*!40000 ALTER TABLE `gmail_memory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `gmail_user_details`
--

LOCK TABLES `gmail_user_details` WRITE;
/*!40000 ALTER TABLE `gmail_user_details` DISABLE KEYS */;
INSERT INTO `gmail_user_details` VALUES (5,'sathiyacse.vit@gmail.com'),(3,'sathiyak@upspring.it');
/*!40000 ALTER TABLE `gmail_user_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'highlight_the_web'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-22 15:00:31

package com.upspring.highlight.dao;

import com.upspring.highlight.domain.Gmail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sindh on 3/28/2017.
 */
public interface HighlightDAO {


    public void gmailMemory(String emailId,String messageId,String messageMode);
    public List<Gmail> getDetails(ArrayList<String> stringArray);
    public void saveUserDetails(String emailId);
    public List<Gmail> checkMessageId(String messageId);

}


package com.upspring.highlight.dao;

import com.upspring.highlight.domain.Gmail;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by sindh on 3/28/2017.
 */
@Repository
public class HighlightDAOImpl implements  HighlightDAO {

    @Autowired(required = false)
    private JdbcTemplate jdbcTemplateObj;

    @Autowired(required = false)
    private NamedParameterJdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override



    public void gmailMemory(String emailId,String messageId,String messageMode)
    {



        String savePageDetailsSql= "INSERT INTO gmail_memory (messageId,email,messageMode) values (:messageId,:emailId,:messageMode)";
        HashMap<String, Object> param = new HashMap<String, Object>();
        param.put("emailId", emailId);
        param.put("messageId", messageId);
        param.put("messageMode",messageMode);
        jdbcTemplate.update(savePageDetailsSql, param);



    }


    public List<Gmail> getDetails(ArrayList<String> stringArray)
    {

        StringBuilder sb = new StringBuilder("Select * from gmail_memory where messageId IN (");
        boolean added = false;
        for(String s:stringArray){
            if (added){
                sb.append(",");
            }
            sb.append("'");
            sb.append(s);
            sb.append("'");
            added = true;
        }
        sb.append(")");




        List<Gmail> userPojectList = null;
        Map<String, Object> parameters = new HashMap<String, Object>();

        userPojectList = jdbcTemplate.query(sb.toString(), parameters, new RowMapper<Gmail>() {

            public Gmail mapRow(ResultSet rs, int rowNum) throws SQLException {

                Gmail project=new Gmail();
                // highlight.setUrl(rs.getString("url"));
                project.setEmailId(rs.getString("email"));
                project.setMessageId(rs.getString("messageId"));
                project.setMessageMode(rs.getString("messageMode"));

                return  project;
            }
        });
        return userPojectList;


    }
    public void saveUserDetails(String emailId)
    {
        String savePageDetailsSql= "INSERT INTO gmail_user_details (emailId) values (:emailId)";
        HashMap<String, Object> param = new HashMap<String, Object>();
        param.put("emailId", emailId);;
        jdbcTemplate.update(savePageDetailsSql, param);

    }
    public List<Gmail> checkMessageId(String messageId)
    {
        String sb="SELECT messageId,messageMode FROM gmail_memory WHERE messageId=:messageId";
        List<Gmail> userPojectList = null;
        HashMap<String, Object> param = new HashMap<String, Object>();
        param.put("messageId", messageId);

        userPojectList = jdbcTemplate.query(sb, param, new RowMapper<Gmail>() {

            public Gmail mapRow(ResultSet rs, int rowNum) throws SQLException {
                Gmail project = new Gmail();
                System.out.println("rs--->"+rs);
                if(rs != null) {

                    // highlight.setUrl(rs.getString("url"));
                    project.setMessageId(rs.getString("messageId"));
                    project.setMessageMode(rs.getString("messageMode"));
                }
                return  project;
            }
        });
        System.out.println("user"+userPojectList);

        return userPojectList;

    }





}




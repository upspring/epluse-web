package com.upspring.highlight.service;

import com.upspring.highlight.dao.HighlightDAO;
import com.upspring.highlight.domain.Gmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sindh on 3/28/2017.
 */
@Service
public class HighlightServiceImpl implements HighlightService {
    @Autowired
    private HighlightDAO highlightDAO;




    @Override

    public void gmailMemory(String emailId,String messagedId,String messageMode){highlightDAO.gmailMemory(emailId,messagedId,messageMode);}
    public List<Gmail> getDetails(ArrayList<String> stringArray){return highlightDAO.getDetails(stringArray);}
    public void saveUserDetails(String emailId){highlightDAO.saveUserDetails(emailId);}
    public List<Gmail> checkMessageId(String messageId){ return  highlightDAO.checkMessageId(messageId);}
    /*public void addUnusedUrl(String url ,String email,String accountType ){ highlightDAO.addUnusedUrl(url,email,accountType);}
    public int getUnusedUrl(String url,String email){ return highlightDAO.getUnusedUrl(url, email);}*/
}


package com.upspring.highlight.controller;

/**
 * Created by sindh on 3/28/2017.
 */

import com.amazonaws.auth.*;
import com.amazonaws.services.comprehend.AmazonComprehend;
import com.amazonaws.services.comprehend.AmazonComprehendClientBuilder;
import com.amazonaws.services.comprehend.model.DetectSentimentRequest;
import com.amazonaws.services.comprehend.model.DetectSentimentResult;


import com.upspring.highlight.domain.Gmail;
import com.upspring.highlight.service.HighlightService;
import org.apache.http.impl.client.CloseableHttpClient;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import com.amazonaws.AmazonClientException;


@Controller

public class HighlightController extends  Thread  {

    @Autowired(required = true)
    private HighlightService highlightService;
    private static final Logger logger = LoggerFactory.getLogger(HighlightController.class);

    private static CloseableHttpClient client;



    private static Integer counter = 1;






    @RequestMapping(value = "/", method = RequestMethod.GET)
    public  String homePage(HttpServletRequest request, HttpSession session) {
        logger.info("index page running");



        //session.setAttribute("latestHighlight", highlightService.latestPublicHighlight());

         return "Home";
        //return "alexaLogin";
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public  String indexPage(HttpServletRequest request, HttpSession session) {
        logger.info("index page running");



        //session.setAttribute("latestHighlight", highlightService.latestPublicHighlight());

        return "Home";
        //return "alexaLogin";
    }


        @RequestMapping(value ="/SentimentAnalysis", method=RequestMethod.POST)
            public @ResponseBody String sentimentAnalysis(HttpServletRequest request,HttpSession session,@RequestParam(value="data",required = true) String data,@RequestParam(value="emailId",required = true ) String emailId,@RequestParam(value="messageId",required = true) String messageId)
        {

        String gmailMode;
        System.setProperty("aws.accessKeyId","AKIAIN6ZFGTY6ESSHCRA");
        System.setProperty("aws.secretKey","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");
        List<Gmail>gm=    highlightService.checkMessageId(messageId);

        System.out.println("gm--->"+gm.size());
        if(gm.size()==0)
        {
         //   AWSCredentialsProvider awsCreds = DefaultAWSCredentialsProviderChain.getInstance();
            AWSCredentialsProvider awsCreds = new AWSCredentialsProviderChain(
                    new SystemPropertiesCredentialsProvider()
                    //new EnvironmentVariableCredentialsProvider(),
                    //new InstanceProfileCredentialsProvider()
            );





            AmazonComprehend comprehendClient = AmazonComprehendClientBuilder.standard()
                    .withCredentials(awsCreds)
                    .withRegion("us-east-1")
                    .build();



            // Call detectSentiment API

            DetectSentimentRequest detectSentimentRequest = new DetectSentimentRequest().withText(data)
                    .withLanguageCode("en");



            DetectSentimentResult detectSentimentResult = comprehendClient.detectSentiment(detectSentimentRequest);

            highlightService.gmailMemory(emailId,messageId,detectSentimentResult.getSentiment());
            gmailMode=detectSentimentResult.getSentiment();

        }
        else
        {
            gmailMode= gm.get(0).getMessageMode();
        }


        //  System.out.println(StringUtils.class.getProtectionDomain().getCodeSource());
       // System.out.println(AwsProfileNameLoader.class.getProtectionDomain().getCodeSource());

       // String text = "It is raining today in Seattle";

        // Create credentials using a provider chain. For more information, see
        // https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/credentials.html

        return gmailMode;
    }


    @RequestMapping (value="/GetDetails",method = RequestMethod.GET)
    public @ResponseBody List<Gmail> getDetails(HttpServletRequest request,HttpServletResponse response,@RequestParam(value="messageIds",required = true) String messageIds)
    {
        System.out.println(messageIds);
        ArrayList<String> stringArray = new ArrayList<String>();

        JSONArray jsonArray = new JSONArray(messageIds);

        for (int i = 0; i < jsonArray.length(); i++) {
            stringArray.add(jsonArray.getString(i));
        }
        System.out.println(stringArray);
        System.out.println(stringArray.size());

        List<Gmail>h=highlightService.getDetails(stringArray);

            return h;
    }



    @RequestMapping(value="/UserDetails",method=RequestMethod.GET)
    public @ResponseBody String saveUsetrdetails(HttpServletRequest request,HttpServletResponse response,@RequestParam(value="emailId",required = true)String emailId)
    {
        highlightService.saveUserDetails(emailId);

        return "succ";
    }





}
